﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mydockerapi.Models
{
    public class TipoCambio
    {
        public Int32 moneda_origen { get; set; }

        public Int32 moneda_destino { get; set; }

        public Double tipo_cambio { get; set; }
    }
}
