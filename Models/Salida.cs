﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mydockerapi.Models
{
    public class Salida
    {
        public long Id { get; set; }

        public Double monto { get; set; }

        private Double _monto_con_tipo_cambio = 0;
        public Double monto_con_tipo_cambio { get {return _monto_con_tipo_cambio; } set { _monto_con_tipo_cambio=value; } }

        public Int32 moneda_origen { get; set; }

        public Int32 moneda_destino { get; set; }

        public Double tipo_cambio { get; set; }
    }
}
