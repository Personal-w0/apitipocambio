﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mydockerapi.Models
{
    public class Entrada
    {
        public Double monto { get; set; }

        public Int32 moneda_origen { get; set; }

        public Int32 moneda_destino { get; set; }
    }
}
