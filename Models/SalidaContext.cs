﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using mydockerapi.Models;

namespace mydockerapi.Models
{
    public class SalidaContext: DbContext
    {
        public SalidaContext(DbContextOptions<SalidaContext> options)
            : base(options)
        {
        }

        public DbSet<Salida> SalidaItems { get; set; }
    }
}
