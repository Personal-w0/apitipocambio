﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.FileProviders;
using mydockerapi.Models;
using mydockerapi.Repositorios;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace mydockerapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConsultaController : ControllerBase
    {
        private readonly SalidaContext _context;

        public ConsultaController(SalidaContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<Salida>>> GetTodoItems()
        {
            return await _context.SalidaItems
                .Select(x => ItemDTO(x))
                .ToListAsync();
        }

        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<Salida>> GetConsulta(long id)
        {
            var todoItem = await _context.SalidaItems.FindAsync(id);

            if (todoItem == null)
            {
                return NotFound();
            }

            return ItemDTO(todoItem);
        }

        [HttpPost("cambiar")]
        [Authorize]
        public async Task<ActionResult<Salida>> CreateConsulta(Entrada item)
        {
            var salida = new Salida
            {
                monto = item.monto,
                moneda_origen = item.moneda_origen,
                moneda_destino = item.moneda_destino,
                monto_con_tipo_cambio = 0,
                tipo_cambio = 0,
                Id = _context.SalidaItems.Count() + 1
            };

            var tc = new RPTipoCambio().ObtenerTipoCambio(item.moneda_origen, item.moneda_destino);
            if (tc != null) {
                salida.monto_con_tipo_cambio = Math.Round(item.monto * tc.tipo_cambio,2);
                salida.tipo_cambio = tc.tipo_cambio;
            }
            else if (item.moneda_origen == item.moneda_destino)
            {
                salida.tipo_cambio = 1;
                salida.monto_con_tipo_cambio = item.monto;
            }

            _context.SalidaItems.Add(salida);
            await _context.SaveChangesAsync();
            
            return CreatedAtAction(
                nameof(GetConsulta),
                new { id = salida.Id },
                ItemDTO(salida));
        }


        private static Salida ItemDTO(Salida item) =>
        new Salida
        {
            Id = item.Id,
            tipo_cambio = item.tipo_cambio,
            monto_con_tipo_cambio = item.monto_con_tipo_cambio,
            moneda_origen = item.moneda_origen,
            moneda_destino = item.moneda_destino,
            monto = item.monto
        };
    }
}
