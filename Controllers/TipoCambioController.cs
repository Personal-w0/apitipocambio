﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using mydockerapi.Models;
using mydockerapi.Repositorios;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace mydockerapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoCambioController : Controller
    {
        [HttpGet]
        [Authorize]
        public IActionResult Get()
        {
            RPTipoCambio rpCli = new RPTipoCambio();
            return Ok(rpCli.ListarTipoCambio());
        }



        [HttpGet("{origen}/{destino}")]
        [Authorize]
        public IActionResult GetTipoCambio(int origen, int destino)
        {
            RPTipoCambio rpTC = new RPTipoCambio();
            var respuesta = rpTC.ObtenerTipoCambio(origen, destino);
            if (respuesta == null)
            {
                var nf = NotFound("El tipo de cambio no está definido.");
                return nf;
            }

            return Ok(respuesta);
        }


        [HttpPost("cambiar")]
        [Authorize]
        public Salida EjecutarTipoCambio(Entrada entrada)
        {
            RPTipoCambio repo = new RPTipoCambio();
            var tc = repo.ObtenerTipoCambio(entrada.moneda_origen, entrada.moneda_destino);

            Salida salida = new Salida();
            salida.tipo_cambio = 0;
            salida.monto_con_tipo_cambio = 0;
            salida.moneda_origen = entrada.moneda_origen;
            salida.moneda_destino = entrada.moneda_destino;
            salida.monto = entrada.monto;

            if (tc != null)
            {
                salida.tipo_cambio = tc.tipo_cambio;
                salida.monto_con_tipo_cambio = Math.Round(entrada.moneda_origen * tc.tipo_cambio, 2);
            }
            else if (entrada.moneda_origen == entrada.moneda_destino)
            {
                salida.tipo_cambio = 1;
                salida.monto_con_tipo_cambio = entrada.monto;
            }
            return salida;
        }

        [HttpPost("actualizar")]
        [Authorize]
        public String ActualizarTipoCambio(TipoCambio entrada)
        {
            return new RPTipoCambio().Update(entrada.moneda_origen, entrada.moneda_destino, entrada.tipo_cambio);
        }

    }
}
