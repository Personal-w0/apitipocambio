﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using mydockerapi.Models;

namespace mydockerapi.Repositorios
{
    public class RPTipoCambio
    {
        public static List<TipoCambio> _listaTipoCambio = new List<TipoCambio>
        {
            new TipoCambio{moneda_origen=1,moneda_destino=2,tipo_cambio=0.33},
            new TipoCambio{moneda_origen=1,moneda_destino=3,tipo_cambio=0.27},
            new TipoCambio{moneda_origen=2,moneda_destino=1,tipo_cambio=3.37},
            new TipoCambio{moneda_origen=2,moneda_destino=3,tipo_cambio=0.91},
            new TipoCambio{moneda_origen=3,moneda_destino=1,tipo_cambio=3.70},
            new TipoCambio{moneda_origen=3,moneda_destino=2,tipo_cambio=1.10},
        };

        public IEnumerable<TipoCambio> ListarTipoCambio()
        {            
            return _listaTipoCambio;
        }

        
        public TipoCambio ObtenerTipoCambio(int origen, int destino)
        {
            var TipoCambio = _listaTipoCambio.Where(tc => tc.moneda_origen == origen && tc.moneda_destino==destino);

            return TipoCambio.FirstOrDefault();
        }

        public string Update(int origen, int destino, double tipo_cambio)
        {
            var existe = false;
            var msj = "";

            if (origen == destino) {
                return "La moneda origen y destino tienen que ser diferentes";
            }

            foreach(var a in _listaTipoCambio)
            {
                if(a.moneda_origen==origen && a.moneda_destino == destino)
                {
                    a.tipo_cambio = tipo_cambio;
                    existe = true;
                }
            }

            if (!existe)
            {
                _listaTipoCambio.Add(new TipoCambio { moneda_origen = origen, moneda_destino = destino, tipo_cambio = tipo_cambio });
                msj = "Nuevo tipo de Cambio Agregado";
            }
            else
            {
                msj = "Tipo de Cambio Actualizado";
            }

            return msj;
        }

    }
}
